# MSWL Bitbucket ALM Tools

## Introduction

Bitbucket ALM Tools analysis for Project Management Subject coursed in [Master on Libre Software (Master Universitario en Software libre)](http://master.libresoft.es/) at [Universidad Rey juan Carlos](http://www.urjc.es/).

## Requirements

Install *LaTeX* and required packages.

`Ubuntu` Installation:

```

#!shell
sudo apt-get install latex-beamer texlive-latex-extra texlive-fonts-extra

```

## Issues

1. [Presentation about Bitbucket](https://bitbucket.org/mswlmanage2013/mswl-bitbucket-alm-tools/issue/1)
2. [Origin, financial support and owners/backup organization(s)](https://bitbucket.org/mswlmanage2013/mswl-bitbucket-alm-tools/issue/2)
3. [Brief summary of relevant statistics (total number of projects, downloads, users, developers, etc.)](https://bitbucket.org/mswlmanage2013/mswl-bitbucket-alm-tools/issue/3)
4. [Most popular projects hosted in that forge.](https://bitbucket.org/mswlmanage2013/mswl-bitbucket-alm-tools/issue/4)
5. [Technical services provided to developers (for either free accounts or pay services)](https://bitbucket.org/mswlmanage2013/mswl-bitbucket-alm-tools/issue/5)
6. [Brief comparison with other forges](https://bitbucket.org/mswlmanage2013/mswl-bitbucket-alm-tools/issue/6)

## Wiki

Develop research analysis information in Wiki repository:

    https://bitbucket.org/mswlmanage2013/mswl-bitbucket-alm-tools/wiki/Home

## Presentations

Slides for presentation of the analysis in classroom.

Directory `slides` has one LaTeX file per section:

```
#!shell

slides/
├── 000-main-bitbucket-analysis.tex
├── 001-bitbucket-presentation.tex
├── 002-bitbucket-origin-financial-support.tex
├── 003-bitbucket-relevant-statics.tex
├── 004-bitbucket-popular-projects.tex
├── 005-bitbucket-technical-services.tex
├── 006-bitbucket-comparison-other-forges.tex
└── 007-bitbucket-references.tex
```

Edit LaTeX files and create final document from Wiki main information.

### Generate pdf

Compile main file to create the final pdf in slide format.

```
#!shell
pdflatex 000-main-bitbucket-analysis.tex
```

Output pdf Slides document - [Bitbucket ALM Tools](https://bitbucket.org/mswlmanage2013/mswl-bitbucket-alm-tools/src/9039bf0054ee01f3981ab38bec88f75a81215e98/000-main-bitbucket-analysis.pdf?at=master)

## License

![Foo](http://i.creativecommons.org/l/by/3.0/88x31.png)

This work is licensed under a [Creative Commons Attribution 3.0 Unported License](http://creativecommons.org/licenses/by/3.0/).

